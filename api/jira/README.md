

# JIRA Extractor
### Extrator de dados da API da atlassian para o Jira. 

## How it works

**Jira Extractor** foi desenvolvido para extrair informações gerais da plataforma Jira, como projetos, _issues_, usuários, _workflows_ entre outros. O extrator possibilita a extração de informações contidas no _endpoint_: https://developer.atlassian.com/cloud/jira/platform/rest/v3/intro/

## Instalação

##### REQUISITOS

- Java 8 +
- Maven
- Git

##### CONSTRUÇÃO

Utilizando o [Maven](https://maven.apache.org/):

- Acesse o diretório no qual os fontes do **jira** se localizam.
- Digite o comando _**mvn package**_.
- O arquivo **jira.jar** será gerado no subdiretório **_target_**.

##### CONFIGURAÇÂO

* Crie um arquivo com o usuário e token gerados na plataforma Jira, este será o seu **credentials file**:

```
{
	"user":"<usuario_que_gerou_o_token>",
	"token":"<token_gerado_na_plataforma_jira>"
}

```

* O documento a seguir auxilia na geração de um token na plataforma Jira: https://developer.atlassian.com/cloud/jira/platform/basic-auth-for-rest-apis/#get-an-api-token

## Utilização

```bash
java -jar jira.jar  \
	--credentials=<Arquivo de credenciais, ou credentials file>  \
	--output=<Arquivo de saída> \
	--field=<Campos que serão colocar no arquivo de saída, esses campos devem vir no resultado da api, deve se passar no formato JsonPath> \
	--endpoint=<Endpoint a ser extraído> \
	--parameters=<(Opcional) Paramêtros do endpoint> \
	--object=<(Opcional) Objeto json que será percorrido> \
	--paginate <Identifica se o endpoint tem um forma de paginação, padrão é 'true'>
	--partition=<(Opcional) Partição, dividida por + se tiver mais de um campo>
	--key=<(Opcional) Chave única, dividida por + se tiver mais de um campo>
```

## Exemplos

### 1: Retornar projetos do Jira

Este exemplo retorna todos os projetos que tem disponível no Jira, a documentação desse _endpoint_ pode ser encontrada em: https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-projects/

```bash
java -jar jira.jar  \
	--credentials="/tmp/path/credentials_file.json" \
	--output="/tmp/path/output_file.csv" \
	--field='expand+self+id+key+name+projectTypeKey+simplified+isPrivate' \
	--endpoint="https://dafiti.jira.com/rest/api/3/project" \
	--partition="::fixed(PROJECTS)" \
	--key="id" \
	--paginate='false' \
```

* O Exemplo acima não possui paginação, então informamos o parâmetro _paginate_ como falso.

### 2: Retornar Issues do Jira

Este exemplo utiliza a linguagem _JQL_ do Jira para efetuar extrações de _issues_ do Jira, com a utilização de _JQL_ fica bem dinâmico a utilização desse extrator, pois podemos extrair o que desejamos e com um filtro bem refirnado também, a documentação deste _endpoint_ pode ser encontrada em: https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-issue-search/#api-rest-api-3-search-get


```bash
java -jar jira.jar  \
	--credentials="/tmp/path/credentials_file.json" \
	--output="/tmp/path/output_file.csv" \
	--field='fields.created>>Date_Created+fields.updated>>Date_Updated+fields.description>>Description+expand+id' \
	--endpoint="https://dafiti.jira.com/rest/api/3/search" \
	--partition="::dateformat(fields.created,yyyy-MM-dd,yyyyMMdd)" \
	--key="id" \
	--object="issues" \
	--parameters='{"jql":"created >= '2022-10-01' AND created <= '2022-10-06'","fields":"created,updated,description"}'
```

* Quando um endpoint possuir paginação, o parâmetro _paginate_ não precisa ser informado.
* O Exemplo acima utiliza o formato JsonPath para pegar alguns campo, por exemplo: _fields.updated_ -> o json de resultado traz o _updated_ dentro do nó _fields_.

## Contributing, Bugs, Questions
Contributions are more than welcome! If you want to propose new changes, fix bugs or improve something feel free to fork the repository and send us a Pull Request. You can also open new `Issues` for reporting bugs and general problems.
