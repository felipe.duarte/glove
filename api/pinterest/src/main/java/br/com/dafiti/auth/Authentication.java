/*
 * Copyright (c) 2022 Dafiti Group
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */
package br.com.dafiti.auth;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.StoredCredential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.http.BasicAuthentication;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fernando Saga
 */
public class Authentication {

    protected String clientId;
    protected String clientSecret;
    protected String tokenPath;
    protected String redirectUri;
    protected int redirectUriPort;
    private static final String TOKEN_SERVER_URL = "https://api.pinterest.com/v5/oauth/token";
    private static final String AUTHORIZATION_SERVER_URL = "https://www.pinterest.com/oauth/";

    private static final Logger LOG = Logger.getLogger(Authentication.class.getName());

    /**
     *
     * @param clientId
     * @param clientSecret
     * @param tokenPath
     * @param redirectUri
     * @param redirectUriPort
     */
    public Authentication(String clientId, String clientSecret, String tokenPath, String redirectUri, int redirectUriPort) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.tokenPath = tokenPath;
        this.redirectUri = redirectUri;
        this.redirectUriPort = redirectUriPort;
    }

    /**
     * App authorization credential to access user's protected data.
     *
     * @return @throws IOException
     */
    public Credential authenticate() throws IOException {
        LOG.log(Level.INFO, "Authenticating with OAuth");

        AuthorizationCodeFlow flow = this.initializeFlow();

        return new AuthorizationCodeInstalledApp(
                flow,
                new LocalServerReceiver.Builder()
                        .setHost(this.redirectUri)
                        .setPort(this.redirectUriPort).build()
        ).authorize("user");
    }

    /**
     * Authorization code flow setup.
     *
     * @return
     * @throws IOException
     */
    public AuthorizationCodeFlow initializeFlow() throws IOException {

        return new AuthorizationCodeFlow.Builder(
                BearerToken.authorizationHeaderAccessMethod(),
                new NetHttpTransport(),
                new GsonFactory(),
                new GenericUrl(TOKEN_SERVER_URL),
                new BasicAuthentication(this.clientId, this.clientSecret),
                this.clientId,
                AUTHORIZATION_SERVER_URL
        ).setCredentialDataStore(StoredCredential.getDefaultDataStore(new FileDataStoreFactory(new File(this.tokenPath)))
        ).setScopes(
                Arrays.asList("ads:read,boards:read,pins:read,user_accounts:read,catalogs:read")
        ).build();
    }
}
