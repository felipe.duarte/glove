/*
 * Copyright (c) 2022 Dafiti Group
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */
package br.com.dafiti.pinterest;

import br.com.dafiti.auth.Authentication;
import br.com.dafiti.mitt.Mitt;
import br.com.dafiti.mitt.cli.CommandLineInterface;
import br.com.dafiti.mitt.model.Configuration;
import br.com.dafiti.mitt.transformation.embedded.Concat;
import br.com.dafiti.mitt.transformation.embedded.Now;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Fernando Saga
 */
public class Pinterest {

    private static final Logger LOG = Logger.getLogger(Pinterest.class.getName());
    private static final String API_URL = "https://api.pinterest.com/v5/";

    public static void main(String[] args) {
        LOG.info("GLOVE - Pinterest API Extractor started");

        JSONObject parameters = null;
        String bookmark = null;
        String redirectUri = "localhost";
        int redirectUriPort = 8085;

        //Define the mitt.
        Mitt mitt = new Mitt();

        try {
            //Defines parameters.
            mitt.getConfiguration()
                    .addParameter("c", "credentials", "Credentials file", "", true, false)
                    .addParameter("o", "output", "Output file", "", true, false)
                    .addParameter("f", "field", "Fields to be retrieved from an endpoint in a JsonPath fashion", "", true, false)
                    .addParameter("e", "endpoint", "Endpoint uri", "", true, false)
                    .addParameter("b", "object", "(Optional) Json object", "", true, true)
                    .addParameter("p", "parameters", "(Optional) Endpoint parameters", "", true, true)
                    .addParameter("l", "lookup", "(Optional) File path to retrieve ID list", "", true, true)
                    .addParameter("a", "partition", "(Optional)  Partition, divided by + if has more than one field", "")
                    .addParameter("k", "key", "(Optional) Unique key, divided by + if has more than one field", "");

            //Reads the command line interface. 
            CommandLineInterface cli = mitt.getCommandLineInterface(args);

            //Defines output file.
            mitt.setOutputFile(cli.getParameter("output"));

            //Defines fields.
            Configuration configuration = mitt.getConfiguration();

            if (cli.hasParameter("partition")) {
                configuration
                        .addCustomField("partition_field", new Concat((List) cli.getParameterAsList("partition", "\\+")));
            }

            if (cli.hasParameter("key")) {
                configuration
                        .addCustomField("custom_primary_key", new Concat((List) cli.getParameterAsList("key", "\\+")));
            }

            configuration
                    .addCustomField("etl_load_date", new Now())
                    .addField(cli.getParameterAsList("field", "\\+"));

            //Reads the credentials file.
            JSONParser parser = new JSONParser();
            JSONObject credentials = (JSONObject) parser.parse(new FileReader(cli.getParameter("credentials")));

            if (credentials.get("redirect_uri") != null) {
                redirectUri = credentials.get("redirect_uri").toString();
            }

            if (credentials.get("redirect_uri_port") != null) {
                redirectUriPort = Integer.parseInt(credentials.get("redirect_uri_port").toString());
            }

            Authentication authentication = new Authentication(
                    credentials.get("client_id").toString(),
                    credentials.get("client_secret").toString(),
                    credentials.get("token_path").toString(),
                    redirectUri,
                    redirectUriPort
            );

            //Retrieve API credentials.
            String token = authentication.authenticate().getAccessToken();

            //Identifies endpoint parameters. 
            String endpointParameter = cli.getParameter("parameters");

            //Identifies if there are a lookup file.
            if (cli.getParameter("lookup") != null) {
                //Identifies the variable that should be replaced.
                Matcher m = Pattern.compile("<<(.*?)>>").matcher(endpointParameter);

                if (m.find()) {
                    String variable = m.group(1);

                    //Gets the replacer value from the lookup file.
                    CsvParserSettings settings = new CsvParserSettings();
                    settings.getFormat().setDelimiter(";");
                    settings.setHeaderExtractionEnabled(true);
                    settings.selectFields(variable.toLowerCase());

                    //Gets just unique values from the lookup column.
                    List<String> lookups = new CsvParser(settings).parseAll(new File(cli.getParameter("lookup")))
                            .stream()
                            .map(item -> item[0])
                            .distinct()
                            .collect(Collectors.toList());

                    //Identifies if it has a lookup.
                    if (lookups != null) {
                        endpointParameter = endpointParameter.replace("<<" + variable + ">>", StringUtils.join(lookups, ","));
                    }
                }
            }

            if (endpointParameter != null && !endpointParameter.isEmpty()) {
                try {
                    parameters = (JSONObject) parser.parse(endpointParameter);
                } catch (ParseException ex) {
                    LOG.log(Level.INFO, "Fail parsing endpoint parameters: {0}", endpointParameter);
                }
            }

            do {
                //Connect to the API. 
                try (CloseableHttpClient client = HttpClients.createDefault()) {
                    HttpGet httpGet = new HttpGet(API_URL + cli.getParameter("endpoint"));

                    //Defines the header.
                    httpGet.addHeader("Content-Type", "application/json");
                    httpGet.setHeader("Accept", "application/json");
                    httpGet.addHeader("Authorization", "Bearer " + token);

                    //Sets default URI parameters. 
                    URIBuilder uriBuilder = new URIBuilder(httpGet.getURI());

                    //Sets endpoint URI parameters. 
                    if (parameters != null && !parameters.isEmpty()) {
                        for (Object k : parameters.keySet()) {
                            uriBuilder.addParameter((String) k, (String) parameters.get(k));
                        }
                    }

                    //API parameter used in pagination.
                    if (bookmark != null) {
                        uriBuilder.addParameter("bookmark", bookmark);
                    }

                    //Sets URI parameters. 
                    httpGet.setURI(uriBuilder.build());

                    //Executes a request. 
                    CloseableHttpResponse response = client.execute(httpGet);

                    //Gets a reponse entity. 
                    String entity = EntityUtils.toString(response.getEntity(), "UTF-8");

                    if (!entity.replace("[]", "").isEmpty()) {
                        Object json;

                        try {
                            json = (JSONArray) new JSONParser().parse(entity);
                        } catch (Exception e) {
                            json = (JSONObject) new JSONParser().parse(entity);
                        }

                        //Identifies if there are payload to process. 
                        if (!json.toString().isEmpty()) {
                            int statusCode = response.getStatusLine().getStatusCode();

                            //Identifies the response status code.
                            switch (statusCode) {
                                case 200 /*OK*/:
                                    try {
                                        bookmark = (String) ((JSONObject) json).get("bookmark");
                                        LOG.log(Level.INFO, "API bookmark: {0}", bookmark);
                                    } catch (Exception e) {
                                        bookmark = null;
                                    }

                                    Object object;

                                    if (cli.getParameter("object") == null || cli.getParameter("object").isEmpty()) {
                                        object = json;
                                    } else {
                                        object = ((JSONObject) json).get(cli.getParameter("object"));
                                    }

                                    if (object instanceof JSONArray) {
                                        ((JSONArray) object).forEach(item -> {
                                            List record = new ArrayList();

                                            mitt.getConfiguration()
                                                    .getOriginalFieldName()
                                                    .forEach(field -> {
                                                        try {
                                                            record.add(JsonPath.read(item, "$." + field));
                                                        } catch (PathNotFoundException ex) {
                                                            record.add("");
                                                        }
                                                    });

                                            mitt.write(record);
                                        });
                                    } else if (object instanceof JSONObject) {
                                        List record = new ArrayList();

                                        mitt.getConfiguration()
                                                .getOriginalFieldName()
                                                .forEach(field -> {
                                                    try {
                                                        record.add(JsonPath.read(object, "$." + field));
                                                    } catch (PathNotFoundException ex) {
                                                        record.add("");
                                                    }
                                                });

                                        mitt.write(record);
                                    }

                                    break;
                                case 404:
                                    throw new Exception("Error 404: " + httpGet.getURI());
                                default:
                                    throw new Exception("HTTP Exception: " + json.toString());
                            }
                        }
                    } else {
                        throw new Exception("Empty response entity for request " + httpGet.getURI());
                    }
                }
            } while (bookmark != null);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Pinterest API extractor fail: ", ex);
            System.exit(1);
        } finally {
            mitt.close();
        }

        LOG.info("Pinterest API Extractor finalized");
    }
}
