# Pinterest API Extractor
### Extrator de dados analíticos do Pinterest REST API.

## How it works
Com o Pinterest REST API é possível consumir dados analíticos para seus pins, anúncios, grupos de anúncios, campanhas ou contas. Os endpoints disponíveis na API estão na seguinte documentação: https://developers.pinterest.com/docs/api/v5/

## Instalação

##### REQUISITOS

- Java 8 +
- Maven
- Git
- Aplicativo de acesso a API cadastrado na plataforma Pinterest (https://developers.pinterest.com/docs/getting-started/set-up-app/)

##### CONSTRUÇÃO

Utilizando o [Maven](https://maven.apache.org/):

- Acesse o diretório no qual os fontes do **pinterest** se localizam.
- Digite o comando _**mvn package**_.
- O arquivo **pinterest.jar** será gerado no subdiretório **_target_**.

##### CONFIGURAÇÃO

* Crie um arquivo com **client id** e **client secret** fornecidos pela Pinterest. Este será o seu **credentials file**:

```
{
	"client_id":"<client_id>",
	"client_secret":"<client_secret>",
	"token_path":"<token_path>",
	"redirect_uri":"<redirect_uri>",
	"redirect_uri_port":"<redirect_uri_port>"
}
```
Detalhando os parâmetros:
* **client_id:** Id do aplicativo Pinterest.
* **client_secret:** Chave secreta do aplicativo Pinterest.
* **token_path:** Local onde ficará o arquivo com o token gerado pela autenticação OAuth.
* **redirect_uri:** URI de redirecionamento registrado em seu aplicativo Pinterest.
* **redirect_uri_port:** Porta da URI de redirecionamento registrado em seu aplicativo Pinterest.

##### Exemplo 1

```
{
	"client_id":"<client_id>",
	"client_secret":"<client_secret>",
	"token_path":"/home/<user>/.pinterest",
	"redirect_uri":"localhost",
	"redirect_uri_port":"8085"
}
```

##### Exemplo 2

Os parâmetros **redirect_uri** e **redirect_uri_port** são opcionais.

```
{
	"client_id":"<client_id>",
	"client_secret":"<client_secret>",
	"token_path":"/home/<user>/.pinterest"
}
```

## Utilização

```bash
java -jar pinterest.jar  \
	--credentials=<Credentials file> \
	--output=<Output file> \
	--field=<Fields to be retrieved from an endpoint in a JsonPath fashion> \
	--endpoint=<Endpoint uri> \
	--object=<(Optional) Json object> \
	--parameters=<(Optional) Endpoint parameters> \
	--lookup=<(Optional) File path to retrieve ID list> \
	--partition=<(Optional) Partition, divided by + if has more than one field> \
	--key=<(Optional) Unique key, divided by + if has more than one field>
```

#### Exemplos
##### Exemplo 1
Obter lista de campanhas de uma conta, endpoint:
https://api.pinterest.com/v5/ad_accounts/{ad_account_id}/campaigns

```bash
java -jar /home/etl/glove/extractor/lib/pinterest.jar  \
	--credentials="/credentials_path/pinterest.json" \
	--output="/tmp/pinterest/campaign/list/list.csv" \
	--field="id+name" \
	--endpoint="ad_accounts/{ad_account_id}/campaigns" \
	--partition="::fixed(FULL)" \
	--key="id" \
	--object="items"
```

##### Exemplo 2
Obter análises de campanhas específicas de uma conta, endpoint:
https://api.pinterest.com/v5/ad_accounts/{ad_account_id}/campaigns/analytics

```bash
java -jar /home/etl/glove/extractor/lib/pinterest.jar  \
	--credentials="/credentials_path/pinterest.json" \
	--output="/tmp/pinterest/campaign/analytics/analytics.csv" \
	--field="DATE+CAMPAIGN_ID+SPEND_IN_DOLLAR+TOTAL_CLICKTHROUGH" \
	--endpoint="ad_accounts/{ad_account_id}/campaigns/analytics" \
	--parameters='{"start_date":"'${STARTDATE}'","end_date":"'${ENDDATE}'","campaign_ids":"626745885078,626746504998","columns":"CAMPAIGN_ID,SPEND_IN_DOLLAR,TOTAL_CLICKTHROUGH","granularity":"DAY"}' \
	--partition="::dateformat(DATE,yyyy-MM-dd,yyyyMM)" \
	--key="::md5([[DATE,CAMPAIGN_ID]])"
```

##### Exemplo 3
Obter análises de todas as campanhas de uma conta, endpoints:
https://api.pinterest.com/v5/ad_accounts/{ad_account_id}/campaigns
https://api.pinterest.com/v5/ad_accounts/{ad_account_id}/campaigns/analytics

```bash
# Campaign list
java -jar /home/etl/glove/extractor/lib/pinterest.jar  \
	--credentials="/credentials_path/pinterest.json" \
	--output="/tmp/pinterest/campaign/list/list.csv" \
	--field="id+name" \
	--endpoint="ad_accounts/{ad_account_id}/campaigns" \
	--partition="::fixed(FULL)" \
	--key="id" \
	--object="items"

# Campaign analytics
java -jar /home/etl/glove/extractor/lib/pinterest.jar  \
	--credentials="/credentials_path/pinterest.json" \
	--output="/tmp/pinterest/campaign/analytics/analytics.csv" \
	--field="DATE+CAMPAIGN_ID+SPEND_IN_DOLLAR+TOTAL_CLICKTHROUGH" \
	--endpoint="ad_accounts/{ad_account_id}/campaigns/analytics" \
	--parameters='{"start_date":"'${STARTDATE}'","end_date":"'${ENDDATE}'","campaign_ids":"<<id>>","columns":"CAMPAIGN_ID,SPEND_IN_DOLLAR,TOTAL_CLICKTHROUGH","granularity":"DAY"}' \
	--partition="::dateformat(DATE,yyyy-MM-dd,yyyyMM)" \
	--key="::md5([[DATE,CAMPAIGN_ID]])" \
	--lookup="/tmp/pinterest/campaign/list/list.csv"
```
Observe que no parametro "parameters" existe o valor "**<<id>>**" que é considerado uma variável dentro do extrator, que indica que essa variável será substituída pelo campo id da extração acima. Para indicar onde está o arquivo gerado na extração acima, é utilizado o parametro **lookup**.
Variável dentro do extrator pode ser qualquer campo (field) que esteja na extração acima, exemplo: \<\<id\>\> ou \<\<name\>\>

## Informações adicionais

Os endpoints que utilizam paginação é possível especificar o **page_size** dentro do parametro **parameters** do extrator. O valor padrão do **page_size** é 25 (registros por página) e o valor máximo é 100.

## Contributing, Bugs, Questions
Contributions are more than welcome! If you want to propose new changes, fix bugs or improve something feel free to fork the repository and send us a Pull Request. You can also open new `Issues` for reporting bugs and general problems.
