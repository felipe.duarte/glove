
# BING API Extractor [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
### Extrator de dados do [BING ADS API](https://docs.microsoft.com/en-us/advertising/guides/?view=bingads-13).

## How it works

O **Bing Ads API Extractor** possibilita extrair as métricas como gastos, conversões, impressões entre outras. Provenientes de campanhas ou ad groups cadastrados na plataforma da microsoft.

## Instalação

##### REQUISITOS

- Java 8 +
- Maven
- Git
- Aplicativo de acesso a API cadastrado na Azure (https://docs.microsoft.com/en-us/advertising/guides/authentication-oauth-register?view=bingads-13).

##### CONSTRUÇÃO

Utilizando o [Maven](https://maven.apache.org/):

- Acesse o diretório no qual os fontes do **bing** se localizam.
- Digite o comando _**mvn package**_.
- O arquivo **bing.jar** será gerado no subdiretório **_target_**.

##### CONFIGURAÇÂO

* Crie um arquivo com o _developer token_, _client id_ disponibilizado pelo Bing e um caminho para armazenar um token temporário. Este será o seu **credentials file**:

```
{
	"developerToken":"<developer_token>",
	"clientId":"<client_id>",
	"refreshTokenPath":"<any_path_to_store_refresh_token/refresh.txt>"
}
```

* A documentação para adquirir um _developer token_ é https://docs.microsoft.com/en-us/advertising/guides/get-started?view=bingads-13#get-developer-token
* O client id estará disponível após o cadastro do app no portal da azure, após esse passo a passo: https://docs.microsoft.com/en-us/advertising/guides/authentication-oauth-register?view=bingads-13
* o refreshTokenPath precisa conter o caminho + nome qualquer.txt para armazenar um token temporário nesse arquivo.

## Utilização

```bash
java -jar bing.jar  \
	--credentials=<Credentials file>  \
	--output=<Output file> \
	--customer=<Identifier of the customer that owns the account.> \
	--start_date=<Start date extraction> \
	--end_date=<End date extraction> \
	--field=<Fields to be retrieved from an endpoint> \
	--account=<(Optional) Sub account id, divided by + if has more than one. If null fetchs Customer id and get all accounts> \
	--aggregation=<(Optional) Aggregation values that you can use for a report. Default: DAILY> \
	--report_name=<Optional) Name of report request. Default: CampaignPerformance> \
	--partition=<(Optional) Partition, divided by + if has more than one field> \
	--key=<(Optional) Unique key, divided by + if has more than one field>
```

* **PS**: Todos os relatórios extraídos do Bing devem conter no mínimo uma métrica e uma dimensão, caso contrário o extrator lançará uma exceção.

## Exemplos

O exemplo abaixo efetua a extração diária de algumas métricas do relatório de **CampaignPerformance**.

```bash
java -jar bing.jar  \
	--credentials="/path/bing.json" \
	--output="/tmp/microsoft/bing/campaign_performance/" \
	--customer="78855666" \
	--start_date="2022-08-25" \
	--end_date="2022-08-26" \
	--field="TimePeriod+AccountName+AccountId+AccountNumber+CampaignName+CampaignId+DeviceType+Spend+Impressions+Clicks+Conversions+Revenue+ImpressionSharePercent" \
	--key="::checksum()" \
	--partition="::dateformat(TimePeriod,yyyy-MM-dd,yyyy-MM-dd)"
```

* **PS**: Nenhum id ou credencial mostrado acima é real, quando efetuar a sua própria extração deve-se obter essas informações direto na plataforma do Bing.


## Contributing, Bugs, Questions
Contributions are more than welcome! If you want to propose new changes, fix bugs or improve something feel free to fork the repository and send us a Pull Request. You can also open new `Issues` for reporting bugs and general problems.
