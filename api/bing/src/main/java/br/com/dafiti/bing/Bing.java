/*
 * Copyright (c) 2022 Dafiti Group
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */
package br.com.dafiti.bing;

import br.com.dafiti.bing.auth.Authentication;
import br.com.dafiti.bing.report.FactoryReport;
import br.com.dafiti.mitt.Mitt;
import br.com.dafiti.mitt.cli.CommandLineInterface;
import br.com.dafiti.mitt.exception.DuplicateEntityException;
import br.com.dafiti.mitt.transformation.embedded.Concat;
import br.com.dafiti.mitt.transformation.embedded.Now;
import com.microsoft.bingads.ApiEnvironment;
import com.microsoft.bingads.AuthorizationData;
import com.microsoft.bingads.ServiceClient;
import com.microsoft.bingads.v13.customermanagement.AdApiFaultDetail_Exception;
import com.microsoft.bingads.v13.customermanagement.AdvertiserAccount;
import com.microsoft.bingads.v13.customermanagement.ApiFault_Exception;
import com.microsoft.bingads.v13.customermanagement.ArrayOfAdvertiserAccount;
import com.microsoft.bingads.v13.customermanagement.ArrayOfPredicate;
import com.microsoft.bingads.v13.customermanagement.ICustomerManagementService;
import com.microsoft.bingads.v13.customermanagement.Paging;
import com.microsoft.bingads.v13.customermanagement.Predicate;
import com.microsoft.bingads.v13.customermanagement.PredicateOperator;
import com.microsoft.bingads.v13.customermanagement.SearchAccountsRequest;
import com.microsoft.bingads.v13.reporting.Report;
import com.microsoft.bingads.v13.reporting.ReportRequest;
import com.microsoft.bingads.v13.reporting.ReportingDownloadParameters;
import com.microsoft.bingads.v13.reporting.ReportingServiceManager;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Helio Leal
 */
public class Bing {

    private static final Logger LOG = Logger.getLogger(Bing.class.getName());

    public static void main(String[] args) {
        //Defines a MITT instance. 
        Mitt mitt = new Mitt();

        try {
            LOG.log(Level.INFO, "Bing Extractor started");

            //Defines parameters.
            mitt.getConfiguration()
                    .addParameter("c", "credentials", "Client credentials", "", true, false)
                    .addParameter("o", "output", "Output file", "", true, false)
                    .addParameter("i", "customer", "Identifier of the customer that owns the account.", "", true, false)
                    .addParameter("s", "start_date", "Start date (format: yyyy-MM-dd)", "", true, false)
                    .addParameter("e", "end_date", "End date (format: yyyy-MM-dd)", "", true, false)
                    .addParameter("f", "field", "Fields to be extracted from the file", "", true, false)
                    .addParameter("f", "account", "(Optional) Sub account id, divided by + if has more than one. If null fetchs Customer id and get all accounts", "")
                    .addParameter("l", "aggregation", "(Optional) Aggregation values that you can use for a report. Default: DAILY", "DAILY")
                    .addParameter("l", "report_name", "(Optional) Name of report request. Default: CampaignPerformance", "CampaignPerformance")
                    .addParameter("p", "partition", "(Optional)  Partition, divided by + if has more than one")
                    .addParameter("k", "key", "(Optional) Unique key, divided by + if has more than one", "");

            //Read the command line interface.
            CommandLineInterface cli = mitt.getCommandLineInterface(args);

            //Defines output file.
            mitt.setOutputFile(cli.getParameter("output"));

            //Defines fields.
            if (cli.hasParameter("partition")) {
                mitt.getConfiguration().addCustomField("partition_field", new Concat((List) cli.getParameterAsList("partition", "\\+")));
            }

            if (cli.hasParameter("key")) {
                mitt.getConfiguration().addCustomField("custom_primary_key", new Concat((List) cli.getParameterAsList("key", "\\+")));
            }

            mitt.getConfiguration()
                    .addCustomField("etl_load_date", new Now())
                    .addField(cli.getParameterAsList("field", "\\+"));

            JSONParser parser = new JSONParser();
            JSONObject credentials = (JSONObject) parser.parse(new FileReader(cli.getParameter("credentials")));

            AuthorizationData authorization = new Authentication(
                    credentials.get("developerToken").toString(),
                    credentials.get("clientId").toString(),
                    credentials.get("refreshTokenPath").toString()).authenticate();
            authorization.setCustomerId(Long.valueOf(cli.getParameter("customer")));

            List<Long> accounts = new ArrayList();

            // If accounts null, automatically retrieves customerId accounts.
            if (cli.getParameter("account") == null) {
                accounts = getCustomerAccounts(Long.valueOf(cli.getParameter("customer")), authorization);

            } else {
                for (String account : cli.getParameterAsList("account", "\\+")) {
                    accounts.add(Long.valueOf(account));
                }
            }

            //Defines the output path. 
            Path outputPath = Files.createTempDirectory("bing_" + UUID.randomUUID());

            for (Long account : accounts) {
                LOG.log(Level.INFO, "Retrieving data from account: {0}", account);

                ReportingDownloadParameters reportingDownloadParameters = new ReportingDownloadParameters();

                ReportRequest reportRequest = FactoryReport.getReportRequest(
                        cli.getParameter("report_name"),
                        cli.getParameter("aggregation"),
                        cli.getParameter("start_date"),
                        cli.getParameter("end_date"),
                        cli.getParameterAsList("field", "\\+"),
                        account);

                reportingDownloadParameters.setReportRequest(reportRequest);
                reportingDownloadParameters.setResultFileDirectory(outputPath.toFile());
                reportingDownloadParameters.setResultFileName(String.valueOf(account) + "_" + UUID.randomUUID());
                reportingDownloadParameters.setOverwriteResultFile(true);

                LOG.log(Level.INFO, "Awaiting download report async...");

                ReportingServiceManager reportingServiceManager;
                reportingServiceManager = new ReportingServiceManager(authorization, ApiEnvironment.PRODUCTION);
                reportingServiceManager.setStatusPollIntervalInMilliseconds(5000);

                Report reportContainer = reportingServiceManager.downloadReportAsync(
                        reportingDownloadParameters,
                        null).get();

                if (reportContainer == null) {
                    LOG.log(Level.WARNING, "There is no report data for the submitted report request parameters.");
                } else {
                    reportContainer.close();
                }
            }

            LOG.log(Level.INFO, "Writing output file to: {0}", cli.getParameter("output"));

            //Writes the final file.
            mitt.getReaderSettings().setDelimiter(',');

            mitt.write(outputPath.toFile());

            //Remove temporary path. 
            Files.delete(outputPath);
        } catch (DuplicateEntityException
                | IOException
                | ParseException
                | AdApiFaultDetail_Exception
                | ApiFault_Exception
                | InterruptedException
                | ExecutionException ex) {

            LOG.log(Level.SEVERE, "Bing Reporting fail: ", ex);
            System.exit(1);
        } finally {
            mitt.close();
            LOG.log(Level.INFO, "Bing Extractor finished");
        }
    }

    private static List<Long> getCustomerAccounts(
            Long customerId,
            AuthorizationData authorization)
            throws ApiFault_Exception, AdApiFaultDetail_Exception, NumberFormatException {
        List<Long> accounts = new ArrayList();

        ArrayOfPredicate predicates = new ArrayOfPredicate();
        Predicate predicate = new Predicate();
        predicate.setField("CustomerId");
        predicate.setOperator(PredicateOperator.EQUALS);
        predicate.setValue("" + customerId);
        predicates.getPredicates().add(predicate);

        Paging paging = new Paging();
        paging.setIndex(0);
        paging.setSize(1000);

        SearchAccountsRequest request = new SearchAccountsRequest();
        request.setPredicates(predicates);
        request.setOrdering(null);
        request.setPageInfo(paging);
        request.setReturnAdditionalFields(null);

        ServiceClient<ICustomerManagementService> service = new ServiceClient<>(
                authorization,
                ApiEnvironment.PRODUCTION,
                ICustomerManagementService.class);

        ArrayOfAdvertiserAccount arrayOfAdvertiserAccount
                = service.getService().searchAccounts(request).getAccounts();

        for (AdvertiserAccount advertiserAccount : arrayOfAdvertiserAccount.getAdvertiserAccounts()) {
            LOG.log(Level.INFO, "Account {0} {1}", new Object[]{advertiserAccount.getName(), advertiserAccount.getId()});
            accounts.add(advertiserAccount.getId());
        }

        return accounts;
    }
}
