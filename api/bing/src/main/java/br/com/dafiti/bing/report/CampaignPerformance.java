/*
 * Copyright (c) 2022 Dafiti Group
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */
package br.com.dafiti.bing.report;

import com.microsoft.bingads.v13.reporting.AccountThroughCampaignReportScope;
import com.microsoft.bingads.v13.reporting.ArrayOfCampaignPerformanceReportColumn;
import com.microsoft.bingads.v13.reporting.ArrayOflong;
import com.microsoft.bingads.v13.reporting.CampaignPerformanceReportColumn;
import com.microsoft.bingads.v13.reporting.CampaignPerformanceReportFilter;
import com.microsoft.bingads.v13.reporting.CampaignPerformanceReportRequest;
import com.microsoft.bingads.v13.reporting.ReportAggregation;
import com.microsoft.bingads.v13.reporting.ReportFormat;
import com.microsoft.bingads.v13.reporting.ReportRequest;
import com.microsoft.bingads.v13.reporting.ReportTime;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 *
 * @author Helio Leal
 */
public class CampaignPerformance {

    private ReportAggregation aggregation;
    private ReportTime reportTime;
    private Long account;
    private List<String> fields;

    public CampaignPerformance(String aggregation, String startDate, String endDate, List<String> fields, Long account) {
        this.aggregation = ReportAggregation.valueOf(aggregation);
        this.reportTime = new ReportTime();
        this.reportTime.setPredefinedTime(null);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        LocalDate start = LocalDate.parse(startDate, formatter);
        com.microsoft.bingads.v13.reporting.Date bingStartDate = new com.microsoft.bingads.v13.reporting.Date();
        bingStartDate.setDay(start.getDayOfMonth());
        bingStartDate.setMonth(start.getMonthValue());
        bingStartDate.setYear(start.getYear());
        reportTime.setCustomDateRangeStart(bingStartDate);

        LocalDate end = LocalDate.parse(endDate, formatter);
        com.microsoft.bingads.v13.reporting.Date bingEndDate = new com.microsoft.bingads.v13.reporting.Date();
        bingEndDate.setDay(end.getDayOfMonth());
        bingEndDate.setMonth(end.getMonthValue());
        bingEndDate.setYear(end.getYear());
        reportTime.setCustomDateRangeEnd(bingEndDate);

        this.fields = fields;
        this.account = account;
    }

    /**
     * Columns should have at least one dimension and one metric, otherwise will
     * throw an error (not easy to understand).
     *
     * @return
     */
    public ReportRequest getReportRequest() {
        CampaignPerformanceReportRequest reportRequest = new CampaignPerformanceReportRequest();

        reportRequest.setAggregation(this.aggregation);
        reportRequest.setExcludeColumnHeaders(false);
        reportRequest.setExcludeReportFooter(true);
        reportRequest.setExcludeReportHeader(true);
        reportRequest.setFormat(ReportFormat.CSV);
        reportRequest.setReturnOnlyCompleteData(false);
        reportRequest.setTime(this.reportTime);
        reportRequest.setReportName("My Campaign Performance Report");

        ArrayOflong accountIds = new ArrayOflong();
        accountIds.getLongs().add(this.account);

        reportRequest.setScope(new AccountThroughCampaignReportScope());
        reportRequest.getScope().setAccountIds(accountIds);
        reportRequest.getScope().setCampaigns(null);

        CampaignPerformanceReportFilter filter = new CampaignPerformanceReportFilter();
        reportRequest.setFilter(filter);

        ArrayOfCampaignPerformanceReportColumn CampaignPerformanceReportColumns = new ArrayOfCampaignPerformanceReportColumn();

        this.fields.forEach((field) -> {
            CampaignPerformanceReportColumns.getCampaignPerformanceReportColumns().add(CampaignPerformanceReportColumn.fromValue(field));
        });

        reportRequest.setColumns(CampaignPerformanceReportColumns);

        return reportRequest;
    }
}
