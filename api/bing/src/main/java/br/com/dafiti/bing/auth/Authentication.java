/*
 * Copyright (c) 2022 Dafiti Group
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */
package br.com.dafiti.bing.auth;

import com.microsoft.bingads.ApiEnvironment;
import com.microsoft.bingads.AuthorizationData;
import com.microsoft.bingads.OAuthDesktopMobileAuthCodeGrant;
import com.microsoft.bingads.OAuthTokens;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The base of code was taken from microsoft docs: https://docs.microsoft.com/en-us/advertising/guides/walkthrough-desktop-application-java?view=bingads-13
 * 
 * @author Helio Leal
 */
public class Authentication {

    protected String developerToken;
    protected String clientId;
    protected String refreshTokenPath;
    protected String clientState = "ClientStateGoesHere";

    private static final Logger LOG = Logger.getLogger(Authentication.class.getName());

    public Authentication(
            String developerToken,
            String clientId,
            String refreshTokenPath) {
        this.developerToken = developerToken;
        this.clientId = clientId;
        this.refreshTokenPath = refreshTokenPath;
    }

    public AuthorizationData authenticate() {
        LOG.log(Level.INFO, "Authenticating with oAuth");

        OAuthDesktopMobileAuthCodeGrant oAuthDesktopMobileAuthCodeGrant = new OAuthDesktopMobileAuthCodeGrant(
                this.clientId,
                ApiEnvironment.PRODUCTION);
        oAuthDesktopMobileAuthCodeGrant.setState(this.clientState);

        AuthorizationData authorizationData = new AuthorizationData();
        authorizationData.setAuthentication(oAuthDesktopMobileAuthCodeGrant);
        authorizationData.setDeveloperToken(this.developerToken);

        try {
            String refreshToken = this.readOAuthRefreshToken();

            if (refreshToken != null) {
                ((OAuthDesktopMobileAuthCodeGrant) (authorizationData.getAuthentication())).requestAccessAndRefreshTokens(refreshToken);
                this.writeOAuthRefreshToken(((OAuthDesktopMobileAuthCodeGrant) (authorizationData.getAuthentication())).getOAuthTokens().getRefreshToken());
            } else {
                this.requestUserConsent(authorizationData);
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Authenticating with oAuth fail: ", ex);
            System.exit(1);
        }

        return authorizationData;
    }

    private void requestUserConsent(AuthorizationData authorizationData) throws Exception, IOException {

        // You must request user consent at least once through a web browser control. 
        LOG.log(Level.WARNING,
                "Open a new web browser and navigate to \n\n{0}\n\n"
                + "Grant consent in the web browser for the application to access "
                + "your advertising accounts, and then enter the response URI that includes "
                + "the authorization 'code' parameter: \n",
                ((OAuthDesktopMobileAuthCodeGrant) (authorizationData.getAuthentication())).getAuthorizationEndpoint());

        Scanner scanner = new Scanner(System.in);
        String responseUri = scanner.nextLine();
        URL url = new URL(responseUri);

        String query = url.getQuery();
        if (!query.contains("state=" + this.clientState)) {
            throw new Exception("The OAuth response state does not match the client request state.");
        }

        OAuthTokens tokens = ((OAuthDesktopMobileAuthCodeGrant) (authorizationData.getAuthentication())).requestAccessAndRefreshTokens(url);
        this.writeOAuthRefreshToken(tokens.getRefreshToken());
    }

    private String readOAuthRefreshToken() throws IOException {
        File file = new File(this.refreshTokenPath);
        String refreshToken = null;

        if (file.exists() && !file.isDirectory()) {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            refreshToken = bufferedReader.readLine();
        }

        return refreshToken;
    }

    private void writeOAuthRefreshToken(String refreshToken) throws IOException {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.refreshTokenPath), "utf-8"))) {
            writer.write(refreshToken);
        }
    }
}
