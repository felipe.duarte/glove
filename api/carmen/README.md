# Carmen API Extractor
### Extrator de dados do Carmen REST API.

## How it works
Com o Carmen REST API é possível consumir dados de campanhas e cupons utilizados na campanha. Temos algumas informações nesse documento: https://github.com/dafiti-group/carmen

## Instalação

##### REQUISITOS

- Java 8 +
- Maven
- Git
- Token gerado na plataforma do Carmen REST API

##### CONSTRUÇÃO

Utilizando o [Maven](https://maven.apache.org/):

- Acesse o diretório no qual os fontes do **carmen** se localizam.
- Digite o comando _**mvn package**_.
- O arquivo **carmen.jar** será gerado no subdiretório **_target_**.

##### CONFIGURAÇÂO

Crie um arquivo com o token fornecido pelo Carmen, este será o seu **credentials file**:

```
{
	"api_key":"<api_key>"
}
```

## Utilização

```bash
java -jar carmen.jar  \
	--credentials=<Credentials file> \
	--output=<Output file> \
	--field=<Fields to be retrieved from an endpoint in a JsonPath fashion> \
	--endpoint=<Endpoint uri> \
	--parameters=<(Optional) Endpoint parameters> \
	--object=<(Optional) Json object> \
	--lookup=<(Optional) File path to retrieve ID list> \
	--dynamic_key=<(Optional) The object has dynamic key, 'true' is default value> \
	--partition=<(Optional) Partition, divided by + if has more than one field> \
	--key=<(Optional) Unique key, divided by + if has more than one field>
```

#### Exemplos
##### Exemplo 1
Obter lista de campanhas, endpoint:<br/>
http://carmen.dafiti.io/campaigns/

```bash
java -jar /home/etl/glove/extractor/lib/carmen.jar \
	--credentials="/credentials_path/carmen.json" \
	--output="/tmp/carmen/campaign/campaign.csv" \
	--field="id+name+created_at+updated_at" \
	--endpoint="campaigns" \
	--partition="::fixed(FULL)" \
	--key="id" \
	--dynamic_key="true" \
	--object="_embedded.items" \
	--parameters='{"updated_since":"2022-01-01","updated_until":"2022-03-31","limit":"100"}'
```

##### Exemplo 2
Obter detalhes de uma campanha específica, endpoint:<br/>
http://carmen.dafiti.io/campaigns/{id}

```bash
java -jar /home/etl/glove/extractor/lib/carmen.jar \
	--credentials="/credentials_path/carmen.json" \
	--output="/tmp/carmen/campaign/campaign.csv" \
	--dynamic_key="false" \
	--field="id+name+discount+rules.MultipleCoupons+created_at+updated_at" \
	--endpoint="campaigns/6058fd6ebeafe71a6876a4c4" \
	--partition="::fixed(FULL)" \
	--key="id"
```

##### Exemplo 3
Obter lista de sellers, endpoint:<br/>
http://carmen.dafiti.io/sellers/

```bash
java -jar /home/etl/glove/extractor/lib/carmen.jar  \
    --credentials="/credentials_path/carmen.json" \
    --output="/tmp/carmen/sellers/sellers.csv" \
    --endpoint="sellers" \
    --dynamic_key="true" \
    --field="id+name+external_id+stores" \
    --parameters='{"paginate":"false"}' \
    --partition="::fixed(FULL)" \
    --key="id"
```

##### Exemplo 4
Obter detalhes de uma lista de campanhas, endpoints:<br/>
http://carmen.dafiti.io/campaigns/<br/>
http://carmen.dafiti.io/campaigns/{id}

```bash
# Campaign list
java -jar /home/etl/glove/extractor/lib/carmen.jar \
    --credentials="/credentials_path/carmen.json" \
    --output="/tmp/carmen/campaign/list/list.csv" \
    --endpoint="campaigns" \
    --object="_embedded.items" \
    --dynamic_key="true" \
    --field="id+name" \
    --parameters='{"updated_since":"'${STARTDATE}'","updated_until":"'${ENDDATE}'","limit":"100"}' \
    --partition="::fixed(FULL)" \
    --key="id"

# Campaign details
java -jar /home/etl/glove/extractor/lib/carmen.jar  \
    --credentials="/credentials_path/carmen.json" \
    --output="/tmp/carmen/campaign/details/details.csv" \
    --endpoint="campaigns/{id}" \
    --dynamic_key="false" \
    --field="id+name+discount+rules.Store+rules.MultipleCoupons+created_at+updated_at" \
    --lookup="/tmp/carmen/campaign/list/list.csv" \
    --partition="::Dateformat(created_at,yyyy-MM-dd'T'HH:mm:ss,yyyyMM)" \
    --key="id"
```
Observe que no parametro "endpoint" existe o valor "**{id}**" que é considerado uma variável dentro do extrator, que indica que essa variável será substituída pelo campo id da extração acima. Para indicar onde está o arquivo gerado na extração acima, é utilizado o parametro **lookup**.
Variável dentro do extrator pode ser qualquer campo (field) que esteja na extração acima, exemplo: {id} ou {name}

Outro parametro bem importante desse extrator é o "**dynamic_key**", que indica se o json de retorno da API utiliza chave dinâmica nos objetos, exemplo:
```bash
{
    "624224fd0231536ef72198b4": {
        "id": "624224fd0231536ef72198b4",
        "name": "Moletom ...",
        ...
    },
    "62bf414159daea3bdf3b6df1": {
        "id": "62bf414159daea3bdf3b6df1",
        "name": "Calça ...",
        ...
    },
    "62bf336b59daea30493aa7f8": {
        "id": "62bf336b59daea30493aa7f8",
        "name": "Camisetas ...",
        ...
    }
}
```

## Contributing, Bugs, Questions
Contributions are more than welcome! If you want to propose new changes, fix bugs or improve something feel free to fork the repository and send us a Pull Request. You can also open new `Issues` for reporting bugs and general problems.
