/*
 * Copyright (c) 2022 Dafiti Group
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */
package br.com.dafiti.carmen;

import br.com.dafiti.mitt.Mitt;
import br.com.dafiti.mitt.cli.CommandLineInterface;
import br.com.dafiti.mitt.model.Configuration;
import br.com.dafiti.mitt.transformation.embedded.Concat;
import br.com.dafiti.mitt.transformation.embedded.Now;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import java.io.File;
import java.io.FileReader;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.text.StringSubstitutor;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Fernando Saga
 */
public class Carmen {

    private static final Logger LOG = Logger.getLogger(Carmen.class.getName());
    private static final String API_URL = "https://carmen.live.dafiti.io";

    /**
     *
     * @param args cli parameteres provided by command line.
     */
    public static void main(String[] args) {
        LOG.info("GLOVE - Carmen API Extractor started");

        JSONObject parameters = null;
        boolean process = true;
        boolean seek = false;
        String variable = null;
        List<String> lookups = null;
        int lookup = 1;

        //Define the mitt.
        Mitt mitt = new Mitt();

        try {
            //Defines parameters.
            mitt.getConfiguration()
                    .addParameter("c", "credentials", "Credentials file", "", true, false)
                    .addParameter("o", "output", "Output file", "", true, false)
                    .addParameter("f", "field", "Fields to be retrieved from an endpoint in a JsonPath fashion", "", true, false)
                    .addParameter("e", "endpoint", "Endpoint uri", "", true, false)
                    .addParameter("b", "object", "(Optional) Json object", "", true, true)
                    .addParameter("d", "dynamic_key", "(Optional) The object has dynamic key", "true")
                    .addParameter("p", "parameters", "(Optional) Endpoint parameters", "", true, true)
                    .addParameter("l", "lookup", "(Optional) File path to retrieve ID list", "", true, true)
                    .addParameter("a", "partition", "(Optional)  Partition, divided by + if has more than one field", "")
                    .addParameter("k", "key", "(Optional) Unique key, divided by + if has more than one field", "");

            //Reads the command line interface. 
            CommandLineInterface cli = mitt.getCommandLineInterface(args);

            //Defines output file.
            mitt.setOutputFile(cli.getParameter("output"));

            //Defines fields.
            Configuration configuration = mitt.getConfiguration();

            if (cli.hasParameter("partition")) {
                configuration
                        .addCustomField("partition_field", new Concat((List) cli.getParameterAsList("partition", "\\+")));
            }

            if (cli.hasParameter("key")) {
                configuration
                        .addCustomField("custom_primary_key", new Concat((List) cli.getParameterAsList("key", "\\+")));
            }

            configuration
                    .addCustomField("etl_load_date", new Now())
                    .addField(cli.getParameterAsList("field", "\\+"));

            //Reads the credentials file.
            JSONParser parser = new JSONParser();
            JSONObject credentials = (JSONObject) parser.parse(new FileReader(cli.getParameter("credentials")));

            //Retrives API credentials. 
            String apiKey = credentials.get("api_key").toString();

            //Identifies endpoint parameters. 
            String endpointParameter = cli.getParameter("parameters");

            if ((endpointParameter != null) && (!endpointParameter.isEmpty())) {
                try {
                    parameters = (JSONObject) parser.parse(endpointParameter);
                } catch (ParseException ex) {
                    LOG.log(Level.INFO, "Fail parsing endpoint parameters: {0}", endpointParameter);
                }
            }

            String endpoint = cli.getParameter("endpoint");

            //Identifies if there are a lookup file.
            if (cli.getParameter("lookup") != null) {
                //Identifies the variable that should be replaced.
                Matcher m = Pattern.compile("\\{(.*?)}").matcher(endpoint);

                if (m.find()) {
                    variable = m.group(1);

                    //Gets the replacer value from the lookup file.
                    CsvParserSettings settings = new CsvParserSettings();
                    settings.getFormat().setDelimiter(";");
                    settings.setHeaderExtractionEnabled(true);
                    settings.selectFields(variable.toLowerCase());

                    //Gets just unique values from the lookup column.
                    lookups = new CsvParser(settings).parseAll(new File(cli.getParameter("lookup")))
                            .stream()
                            .map(item -> item[0])
                            .distinct()
                            .collect(Collectors.toList());
                }
            }

            //Defines the path variable replacer.
            Map<String, String> pathVariable = new HashMap<>();
            StringSubstitutor substitutor = new StringSubstitutor(pathVariable).setVariablePrefix("{");

            String requestUrl = API_URL + "/" + endpoint;

            LOG.log(Level.INFO, "Request URL: {0}", requestUrl);

            do {
                //Connect to the API. 
                try (CloseableHttpClient client = getCloseableHttpClient()) {

                    //Identifies if it has a lookup.
                    if (lookups != null) {
                        LOG.log(Level.INFO, "Requesting detail of {0} ({1} of {2})", new Object[]{variable, lookup, lookups.size()});
                        pathVariable.put(variable, lookups.get(lookup - 1));
                        lookup++;
                        seek = (lookups.size() >= lookup);
                    }

                    HttpGet httpGet = new HttpGet(substitutor.replace(requestUrl));

                    //Defines the header.
                    httpGet.addHeader("Content-Type", "application/json");
                    httpGet.setHeader("Accept", "application/json");
                    httpGet.addHeader("Authorization", apiKey);

                    //Sets default URI parameters. 
                    URIBuilder uriBuilder = new URIBuilder(httpGet.getURI());

                    //Sets endpoint URI parameters. 
                    if ((parameters != null) && (!parameters.isEmpty())) {
                        for (Object k : parameters.keySet()) {
                            uriBuilder.addParameter((String) k, (String) parameters.get(k));
                        }
                        //Add parameters in uriBuilder only once.
                        parameters = null;
                    }

                    //Sets URI parameters. 
                    httpGet.setURI(uriBuilder.build());

                    //Executes a request. 
                    CloseableHttpResponse response = client.execute(httpGet);

                    //Gets a reponse entity. 
                    String entity = EntityUtils.toString(response.getEntity(), "UTF-8");

                    if (!entity.replace("[]", "").isEmpty()) {
                        Object json;

                        try {
                            json = (JSONArray) new JSONParser().parse(entity);
                        } catch (Exception e) {
                            json = (JSONObject) new JSONParser().parse(entity);
                        }

                        //Identifies if there are payload to process. 
                        if (!json.toString().isEmpty()) {
                            int statusCode = response.getStatusLine().getStatusCode();

                            //Identifies the response status code.
                            switch (statusCode) {
                                case 200 /*OK*/:
                                    Object object = json;

                                    try {
                                        //Display page statistics.
                                        long page = JsonPath.read(json, "$.page");
                                        long limit = JsonPath.read(json, "$.limit");
                                        long pages = JsonPath.read(json, "$.pages");
                                        long total = JsonPath.read(json, "$.total");

                                        LOG.log(Level.INFO, "Total records found: {0} | Page: {1}/{2} ({3} per page)",
                                                new Object[]{total, page, pages, limit}
                                        );

                                        //API pagination.
                                        requestUrl = API_URL + JsonPath.read(object, "$._links.next.href");
                                    } catch (Exception ex) {
                                        process = false;
                                    }

                                    String objectParameter = cli.getParameter("object");

                                    if ((objectParameter != null) && (!objectParameter.isEmpty())) {
                                        object = JsonPath.read(object, "$." + objectParameter);
                                    }

                                    if (ObjectUtils.isNotEmpty(object)) {
                                        //Identifies if the object contains dynamic key.
                                        if (cli.getParameterAsBoolean("dynamic_key")) {
                                            //Get dynamic keys from object.
                                            Set keySet = ((JSONObject) object).keySet();

                                            for (Object key : keySet) {
                                                List record = new ArrayList();

                                                for (String field : mitt.getConfiguration().getOriginalFieldName()) {
                                                    try {
                                                        record.add(JsonPath.read(object, "$." + key + "." + field));
                                                    } catch (PathNotFoundException ex) {
                                                        record.add("");
                                                    }
                                                }

                                                mitt.write(record);
                                            }
                                        } else {
                                            if (object instanceof JSONArray) {
                                                ((JSONArray) object).forEach(item -> {
                                                    List record = new ArrayList();

                                                    for (String field : mitt.getConfiguration().getOriginalFieldName()) {
                                                        try {
                                                            record.add(JsonPath.read(item, "$." + field));
                                                        } catch (PathNotFoundException ex) {
                                                            record.add("");
                                                        }
                                                    }

                                                    mitt.write(record);
                                                });
                                            } else if (object instanceof JSONObject) {
                                                List record = new ArrayList();

                                                for (String field : mitt.getConfiguration().getOriginalFieldName()) {
                                                    try {
                                                        record.add(JsonPath.read(object, "$." + field));
                                                    } catch (PathNotFoundException ex) {
                                                        record.add("");
                                                    }
                                                }

                                                mitt.write(record);
                                            }
                                        }
                                    } else {
                                        process = false;
                                    }

                                    break;
                                case 404:
                                    throw new Exception("Error 404: " + httpGet.getURI());
                                default:
                                    throw new Exception("HTTP Exception: " + json.toString());
                            }
                        } else {
                            process = false;
                        }
                    } else {
                        throw new Exception("Empty response entity for request " + httpGet.getURI());
                    }
                }
            } while (process || seek);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Carmen API Extractor fail: ", ex);
            System.exit(1);
        } finally {
            mitt.close();
        }
        LOG.info("Carmen API Extractor finalized");
    }

    /**
     * Bypass SSL Certificate Checking using CloseableHttpClient.
     *
     * @return
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.KeyStoreException
     * @throws java.security.KeyManagementException
     */
    public static CloseableHttpClient getCloseableHttpClient() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        CloseableHttpClient httpClient = null;

        httpClient = HttpClients.custom()
                .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
                    @Override
                    public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                        return true;
                    }
                }).build()).build();

        return httpClient;
    }
}
